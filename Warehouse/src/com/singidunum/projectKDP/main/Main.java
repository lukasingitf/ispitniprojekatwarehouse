/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.main;

import com.singidunum.projectKDP.data.Customers;
import com.singidunum.projectKDP.data.Employees;
import com.singidunum.projectKDP.data.Products;
import com.singidunum.projectKDP.data.Shippers;
import com.singidunum.projectKDP.data.Suppliers;
import com.singidunum.projectKDP.exception.WarehouseException;
import com.singidunum.projectKDP.service.AdvancedService;
import com.singidunum.projectKDP.service.OrderService;
import com.singidunum.projectKDP.service.ProductsService;
import com.singidunum.projectKDP.service.ShippingSideService;
import java.sql.SQLException;

/**
 *
 * @author Uros
 */
public class Main {
    
    private static final OrderService orderService = OrderService.getInstance();
    private static final ShippingSideService shippingSideService = ShippingSideService.getInstance();
    private static final ProductsService productService = ProductsService.getInstance();
    private static final AdvancedService advanced= AdvancedService.getInstance();
    
    
    public static void main(String[] args) throws WarehouseException, SQLException {
//        addNewCustomers();
//        addNewProducts();
      //    addNewOrder();
        //  advanced.treca(6);
           //productService.deleteProduct(2);
          advanced.deseta();
          
          
    }
    
    
    public static void addNewCustomers () throws WarehouseException{
        shippingSideService.addNewCustomer(new Customers("Let's Stop N Shop", "Jaime Yorres", "87 Polk St. Suite 5", "Beograd", 11000, "Srbija"));
        shippingSideService.addNewShipper(new Shippers("Camil", "0655635632"));
        shippingSideService.addNewEmployee(new Employees("Mikic", "Mika", "12.12.1965."));
    }
    
    public static void addNewProducts() throws WarehouseException{
        productService.addNewProductSupplier(new Suppliers("Misa suplajer", "Misa Miskovic", "Ace Pejica 23", "Mala Mostanica", "Srbija", 45099, "063445554"));
        productService.addNewProduct(new Products("LG 231 Sonic", "TV", 580, productService.findSupplier(1)));
    }
    
    public static void addNewOrder() throws WarehouseException{
        orderService.makeOrder(shippingSideService.findCustomer(9), shippingSideService.findShipper(5), shippingSideService.findEmployee(5), productService.findProduct(8), 2, "12.6.2017.");
        orderService.makeOrder(shippingSideService.findCustomer(9), shippingSideService.findShipper(6), shippingSideService.findEmployee(5), productService.findProduct(8), 20, "12.6.2017.");
        orderService.makeOrder(shippingSideService.findCustomer(9), shippingSideService.findShipper(7), shippingSideService.findEmployee(4), productService.findProduct(7), 29, "13.6.2017.");
        orderService.makeOrder(shippingSideService.findCustomer(9), shippingSideService.findShipper(8), shippingSideService.findEmployee(4), productService.findProduct(6), 82, "18.6.2017.");
        
    }
    
}
