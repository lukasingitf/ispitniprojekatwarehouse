/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.service;

import com.singidunum.projectKDP.dao.CustomersDao;
import com.singidunum.projectKDP.dao.EmployeesDao;
import com.singidunum.projectKDP.dao.ResourcesManager;
import com.singidunum.projectKDP.dao.ShippersDao;
import com.singidunum.projectKDP.data.Customers;
import com.singidunum.projectKDP.data.Employees;
import com.singidunum.projectKDP.data.Shippers;
import com.singidunum.projectKDP.exception.WarehouseException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author Uros
 */
public class ShippingSideService {
    
    private static  ShippingSideService instance=null;

    public  ShippingSideService() {
    }
    
    public static  ShippingSideService getInstance(){
        if(instance == null){
            instance= new  ShippingSideService();
        }
        return instance;
    }
    
    public void addNewCustomer(Customers c) throws WarehouseException {
            Connection con = null;
        
        try{
            con = ResourcesManager.getConnection();
            CustomersDao.getInstance().insert(c, con);
        }catch (SQLException e){
            throw new WarehouseException("Ne moze se dodati customer.");
        }  finally {
            ResourcesManager.closeConnection(con);
        }
    }
    
    public Customers findCustomer (int idCustomer) throws WarehouseException {
        Connection con = null;
        try {
            con = ResourcesManager.getConnection();
            return CustomersDao.getInstance().find(idCustomer, con);
        } catch (SQLException ex) {
            throw new WarehouseException("Ne moze se pronaci customer.");
        }
    }
    
    public void updateCustomer(Customers c) throws WarehouseException {
            Connection con = null;
        
        try{
            con = ResourcesManager.getConnection();
            CustomersDao.getInstance().update(c, con);
        }catch (SQLException e){
            throw new WarehouseException("Ne moze se update-ovati customer.");
        }  finally {
            ResourcesManager.closeConnection(con);
        }
    }
    
    public void deleteCustomer(int idCustomer) throws WarehouseException {
            Connection con = null;
        
        try{
            con = ResourcesManager.getConnection();
            CustomersDao.getInstance().delete(idCustomer, con);
        }catch (SQLException e){
            throw new WarehouseException("Ne moze se obrisati customer.");
        }  finally {
            ResourcesManager.closeConnection(con);
        }
    }
    
    public void addNewShipper(Shippers s) throws WarehouseException {
            Connection con = null;
        
        try{
            con = ResourcesManager.getConnection();
            ShippersDao.getInstance().insert(s, con);
        }catch (SQLException e){
            throw new WarehouseException("Ne moze se dodati shipper.");
        }  finally {
            ResourcesManager.closeConnection(con);
        }
    }
    
    public Shippers findShipper (int idShipper) throws WarehouseException {
        Connection con = null;
        try {
            con = ResourcesManager.getConnection();
            return ShippersDao.getInstance().find(idShipper, con);
        } catch (SQLException ex) {
            throw new WarehouseException("Ne moze se pronaci shipper.");
        }
    }
    
    public void updateCustomer(Shippers c) throws WarehouseException {
            Connection con = null;
        
        try{
            con = ResourcesManager.getConnection();
            ShippersDao.getInstance().update(c, con);
        }catch (SQLException e){
            throw new WarehouseException("Ne moze se update-ovati customer.");
        }  finally {
            ResourcesManager.closeConnection(con);
        }
    }
    
    public void deleteShipper(int idShipper) throws WarehouseException {
            Connection con = null;
        
        try{
            con = ResourcesManager.getConnection();
            ShippersDao.getInstance().delete(idShipper, con);
        }catch (SQLException e){
            throw new WarehouseException("Ne moze se obrisati shipper.");
        }  finally {
            ResourcesManager.closeConnection(con);
        }
    }
    
     public void addNewEmployee(Employees emp) throws WarehouseException {
            Connection con = null;
        
        try{
            con = ResourcesManager.getConnection();
            EmployeesDao.getInstance().insert(emp, con);
        }catch (SQLException e){
            throw new WarehouseException("Ne moze se dodati employee.");
        }  finally {
            ResourcesManager.closeConnection(con);
        }
    }
    
    public Employees findEmployee (int idEmployee) throws WarehouseException {
        Connection con = null;
        try {
            con = ResourcesManager.getConnection();
            return EmployeesDao.getInstance().find(idEmployee, con);
        } catch (SQLException ex) {
            throw new WarehouseException("Ne moze se pronaci employee.");
        }
    }
    
    public void updateEmployee(Employees emp) throws WarehouseException {
            Connection con = null;
        
        try{
            con = ResourcesManager.getConnection();
            EmployeesDao.getInstance().update(emp, con);
        }catch (SQLException e){
            throw new WarehouseException("Ne moze se update-ovati employee.");
        }  finally {
            ResourcesManager.closeConnection(con);
        }
    }
    
    public void deleteEmployee(int idEmployee) throws WarehouseException {
            Connection con = null;
        
        try{
            con = ResourcesManager.getConnection();
            EmployeesDao.getInstance().delete(idEmployee, con);
        }catch (SQLException e){
            throw new WarehouseException("Ne moze se obrisati employee.");
        }  finally {
            ResourcesManager.closeConnection(con);
        }
    }
   
}
