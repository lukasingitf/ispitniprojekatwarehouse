/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.service;

import com.singidunum.projectKDP.dao.OrderDetailsDao;
import com.singidunum.projectKDP.dao.OrdersDao;
import com.singidunum.projectKDP.dao.ProductsDao;
import com.singidunum.projectKDP.dao.ResourcesManager;
import com.singidunum.projectKDP.dao.SuppliersDao;
import com.singidunum.projectKDP.data.OrderDetails;
import com.singidunum.projectKDP.data.Products;
import com.singidunum.projectKDP.data.Suppliers;
import com.singidunum.projectKDP.exception.WarehouseException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author Uros
 */
public class ProductsService {
    
     private static  ProductsService instance=null;

    public  ProductsService() {
    }
    
    public static  ProductsService getInstance(){
        if(instance == null){
            instance= new  ProductsService();
        }
        return instance;
    }
    
    public int addNewProduct (Products p) throws WarehouseException{
        Connection con = null;
        
        try{
            con = ResourcesManager.getConnection();
            return ProductsDao.getInstance().insert(p, con);
            
            
        } catch (SQLException ex) {
            throw new WarehouseException("Ne moze se dodati novi product.");
        } finally {
            ResourcesManager.closeConnection(con);
        }
    }
    
    public void addNewProductSupplier(Suppliers s) throws WarehouseException {
            Connection con = null;
        
        try{
            con = ResourcesManager.getConnection();
            SuppliersDao.getInstance().insert(s, con);
        }catch (SQLException e){
            throw new WarehouseException("Ne moze se dodati supplier.");
        }  finally {
            ResourcesManager.closeConnection(con);
        }
    }
    
     public Products findProduct(int idProduct) throws WarehouseException{
        Connection con= null;
        try{
            con = ResourcesManager.getConnection();
            return ProductsDao.getInstance().find(idProduct, con);
            
        } catch (SQLException ex) {
           throw new WarehouseException("Ne moze se pronaci product.");
        } finally {
            ResourcesManager.closeConnection(con);
        }
    }
     
     public Suppliers findSupplier (int idSupplier) throws WarehouseException {
        Connection con = null;
        try {
            con = ResourcesManager.getConnection();
            return SuppliersDao.getInstance().find(idSupplier, con);
        } catch (SQLException ex) {
            throw new WarehouseException("Ne moze se pronaci supplier.");
        }
    }
     
    public void updateProduct(Products p) throws WarehouseException {
        Connection con = null;
        try {
            con = ResourcesManager.getConnection();
            con.setAutoCommit(false);
            ProductsDao.getInstance().update(p, con);
            con.commit();
            
        } catch (SQLException ex) {
            ResourcesManager.rollbackTransactions(con);
            throw new WarehouseException("Ne moze se update-ovati product");
        } finally {
            ResourcesManager.closeConnection(con);
        }
    }
    
    public void updateSupplier(Suppliers s) throws WarehouseException {
            Connection con = null;
        
        try{
            con = ResourcesManager.getConnection();
            SuppliersDao.getInstance().update(s, con);
        }catch (SQLException e){
            throw new WarehouseException("Ne moze se update-ovati supplier.");
        }  finally {
            ResourcesManager.closeConnection(con);
        }
    }
    
    public void deleteProduct(int idProduct) throws WarehouseException {
        Connection conn = null;
        
        try {
            conn = ResourcesManager.getConnection(); 
            conn.setAutoCommit(false);
            
            OrderDetails orderDetails = OrderDetailsDao.getInstance().findFkProduct(idProduct, conn);
            
            if(orderDetails!=null){
                OrdersDao.getInstance().deleteFkProduct(orderDetails.getOrder().getOrderID(), conn);
            }
            Products p = ProductsDao.getInstance().find(idProduct, conn);
            
            if(p != null) {
                ProductsDao.getInstance().delete(p, conn);
            }
            
            
            conn.commit();
        } catch (SQLException e) {
            ResourcesManager.rollbackTransactions(conn);
            System.out.println(e);
            throw new WarehouseException("Ne moze se obrisati product.");
        } finally {
            ResourcesManager.closeConnection(conn);
        }
    }
    
    public void deleteSupplier(int idSupplier) throws WarehouseException {
            Connection con = null;
        
        try{
            con = ResourcesManager.getConnection();
            SuppliersDao.getInstance().delete(idSupplier, con);
        }catch (SQLException e){
            throw new WarehouseException("Ne moze se obrisati supplier.");
        }  finally {
            ResourcesManager.closeConnection(con);
        }
    }
      
      
    
}
