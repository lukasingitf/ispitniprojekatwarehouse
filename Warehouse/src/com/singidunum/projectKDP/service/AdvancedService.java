/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.service;

import com.singidunum.projectKDP.dao.ProductsDao;
import com.singidunum.projectKDP.dao.ResourcesManager;
import com.singidunum.projectKDP.data.Products;
import com.singidunum.projectKDP.exception.WarehouseException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


/**
 *
 * @author Uros
 */
public class AdvancedService {
    
     private static  AdvancedService instance=null;

    public  AdvancedService() {
    }
    
    public static  AdvancedService getInstance(){
        if(instance == null){
            instance= new  AdvancedService();
        }
        return instance;
    }
    
    public void prva() throws WarehouseException, SQLException  {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder sb= new StringBuilder();
        try {
            con = ResourcesManager.getConnection();
            ps = con.prepareStatement("SELECT customers.customer_name, orders.id_order"
                    + " FROM customers, orders WHERE customers.id_customer=fk_customer"
                    + " ORDER BY customer_name ASC;");
            
            rs = ps.executeQuery();
            while(rs.next()){
                sb.append(rs.getString("customer_name")).append("\t").append(rs.getInt("id_order")).append("\n");
            }
            System.out.println(sb);
        } catch (SQLException ex) {
             System.out.println(ex);
             throw new WarehouseException("Ne moze se uraditi select nad customers i orders.");
        } finally {
            ResourcesManager.closeResources(rs, ps);
            ResourcesManager.closeConnection(con);
        }
    }
    
    public void druga(int idSupplier) throws WarehouseException{
         
        Connection con= null;
        StringBuilder sb=new StringBuilder();
        try{
            con = ResourcesManager.getConnection();
           List<Products> productsList= ProductsDao.getInstance().findAllProductSupplier(idSupplier, con);
            for (Products product : productsList) {
                sb.append(product.getProductName()).append("\t").append(product.getSupplier().getSupplierID());
                sb.append("\n");
            }
            System.out.println(sb);
        } catch (SQLException ex) {
           throw new WarehouseException("Ne moze se pronaci product.");
        } finally {
            ResourcesManager.closeConnection(con);
        }
    }
    
    public void treca(int idShipper) throws WarehouseException, SQLException  {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder sb= new StringBuilder();
        try {
            con = ResourcesManager.getConnection();
            ps = con.prepareStatement("SELECT * FROM products INNER JOIN "
                    + "(order_details INNER JOIN "
                    + "(orders INNER JOIN shippers ON orders.fk_shipper=shippers.id_shipper)"
                    + " ON order_details.fk_order=orders.id_order)"
                    + " ON products.id_product=order_details.fk_product"
                    + " WHERE id_shipper=?;");
            ps.setInt(1, idShipper);
            rs = ps.executeQuery();
            while(rs.next()){
                sb.append(rs.getString("product_name")).append("\t").append(rs.getInt("id_shipper")).append("\n");
            }
            System.out.println(sb);
        } catch (SQLException ex) {
             System.out.println(ex);
             throw new WarehouseException("Ne moze se uraditi select za trecu metodu.");
        } finally {
            ResourcesManager.closeResources(rs, ps);
            ResourcesManager.closeConnection(con);
        }
    }
    
    public void cetvrta() throws WarehouseException, SQLException  {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder sb= new StringBuilder();
        int narudzbineUkCena=0;
        try {
            con = ResourcesManager.getConnection();
            ps = con.prepareStatement("SELECT price_per_unit, quantity FROM products "
                    + "INNER JOIN order_details ON products.id_product=order_details.fk_product;");
            
            rs = ps.executeQuery();
            while(rs.next()){
                narudzbineUkCena=narudzbineUkCena+(rs.getInt("price_per_unit")*rs.getInt("quantity"));
            }
            sb.append("Cena svih narudzbina je: ").append(narudzbineUkCena);
            System.out.println(sb);
        } catch (SQLException ex) {
             System.out.println(ex);
             throw new WarehouseException("Ne moze se uraditi select nad products i order_details.");
        } finally {
            ResourcesManager.closeResources(rs, ps);
            ResourcesManager.closeConnection(con);
        }
    }
    
    public void peta(int idCustomer) throws WarehouseException, SQLException  {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder sb= new StringBuilder();
        int narudzbineCena=0;
        try {
            con = ResourcesManager.getConnection();
            ps = con.prepareStatement("SELECT * FROM customers "
                    + "INNER JOIN (orders INNER JOIN (order_details INNER JOIN products"
                    + " ON products.id_product=order_details.fk_product)"
                    + " ON orders.id_order=order_details.fk_order) "
                    + " ON customers.id_customer=orders.fk_customer "
                    + "WHERE id_customer=?;");
            ps.setInt(1, idCustomer);
            rs = ps.executeQuery();
            while(rs.next()){
                narudzbineCena=narudzbineCena+(rs.getInt("price_per_unit")*rs.getInt("quantity"));
            }
            sb.append("Cena svih narudzbina koje je narucio customer: ").append(idCustomer).append(" je ").append(narudzbineCena);
            System.out.println(sb);
        } catch (SQLException ex) {
             System.out.println(ex);
             throw new WarehouseException("Ne moze se uraditi select za petu metodu.");
        } finally {
            ResourcesManager.closeResources(rs, ps);
            ResourcesManager.closeConnection(con);
        }
    }
    
    public void sesta(int idShipper) throws WarehouseException, SQLException  {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder sb= new StringBuilder();
        int narudzbineCena=0;
        String shipper= null;
        try {
            con = ResourcesManager.getConnection();
            ps = con.prepareStatement("SELECT * FROM shippers "
                    + "INNER JOIN (orders INNER JOIN (order_details INNER JOIN products"
                    + " ON products.id_product=order_details.fk_product)"
                    + " ON orders.id_order=order_details.fk_order) "
                    + " ON shippers.id_shipper=orders.fk_shipper "
                    + "WHERE id_shipper=?;");
            ps.setInt(1, idShipper);
            rs = ps.executeQuery();
            while(rs.next()){
                narudzbineCena=narudzbineCena+(rs.getInt("price_per_unit")*rs.getInt("quantity"));
                shipper=rs.getString("shipper_name");
            }
            sb.append("Cena svih narudzbina koje je dostavio: ").append(shipper).append(" je ").append(narudzbineCena);
            System.out.println(sb);
        } catch (SQLException ex) {
             System.out.println(ex);
             throw new WarehouseException("Ne moze se uraditi select za sestu metodu.");
        } finally {
            ResourcesManager.closeResources(rs, ps);
            ResourcesManager.closeConnection(con);
        }
    }
   
    public void sedma(int idSupplier) throws WarehouseException, SQLException  {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder sb= new StringBuilder();
        int narudzbineCena=0;
        String supplier= null;
        try {
            con = ResourcesManager.getConnection();
            ps = con.prepareStatement("SELECT * FROM suppliers INNER JOIN "
                    + "(products INNER JOIN order_details ON products.id_product=order_details.fk_product)"
                    + " ON suppliers.id_supplier=products.fk_supplier"
                    + " WHERE id_supplier=?;");
            ps.setInt(1, idSupplier);
            rs = ps.executeQuery();
            while(rs.next()){
                narudzbineCena=narudzbineCena+(rs.getInt("price_per_unit")*rs.getInt("quantity"));
                supplier=rs.getString("supplier_name");
            }
            sb.append("Cena svih narudzbina koje je dostavio: ").append(supplier).append(" je ").append(narudzbineCena);
            System.out.println(sb);
        } catch (SQLException ex) {
             System.out.println(ex);
             throw new WarehouseException("Ne moze se uraditi select za sedmu metodu.");
        } finally {
            ResourcesManager.closeResources(rs, ps);
            ResourcesManager.closeConnection(con);
        }
    }
    
    
     public void osma() throws WarehouseException, SQLException  {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder sb= new StringBuilder();
        int narudzbineCena=0;
        String employee= null;
        int max= Integer.MIN_VALUE;
        try {
            con = ResourcesManager.getConnection();
            ps = con.prepareStatement("SELECT * FROM employees "
                    + "INNER JOIN (orders INNER JOIN (order_details INNER JOIN products"
                    + " ON products.id_product=order_details.fk_product)"
                    + " ON orders.id_order=order_details.fk_order) "
                    + " ON employees.id_employee=orders.fk_employee;");
        
            rs = ps.executeQuery();
            while(rs.next()){
                if(rs.getInt("quantity")*rs.getInt("price_per_unit")>max){
                    employee=rs.getString("last_name")+" "+rs.getString("first_name");
                }
            }
            sb.append("Zaposleni kod koga je narucena najveca vrednost robe je: ").append(employee);
            System.out.println(sb);
        } catch (SQLException ex) {
             System.out.println(ex);
             throw new WarehouseException("Ne moze se uraditi select za osmu metodu.");
        } finally {
            ResourcesManager.closeResources(rs, ps);
            ResourcesManager.closeConnection(con);
        }
    }
     
    public void deveta() throws WarehouseException, SQLException  {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder sb= new StringBuilder();
        try {
            con = ResourcesManager.getConnection();
            ps = con.prepareStatement("SELECT COUNT(fk_product) AS ukupno, product_name "
                    + "FROM products INNER JOIN order_details ON products.id_product=order_details.fk_product"
                    + " GROUP BY product_name ORDER BY ukupno DESC LIMIT 2;");
            rs = ps.executeQuery();
            while(rs.next()){
                sb.append(rs.getString("product_name")).append("\t").append(rs.getInt("ukupno")).append("\n");
            }
            System.out.println(sb);
        } catch (SQLException ex) {
             System.out.println(ex);
             throw new WarehouseException("Ne moze se uraditi select za devetu metodu.");
        } finally {
            ResourcesManager.closeResources(rs, ps);
            ResourcesManager.closeConnection(con);
        }
    }
    
    public void deseta() throws WarehouseException, SQLException  {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder sb= new StringBuilder();
        try {
            con = ResourcesManager.getConnection();
            ps = con.prepareStatement("SELECT SUM(quantity*price_per_unit) AS ukupno, customer_name "
                    + "FROM customers "
                    + "INNER JOIN (orders INNER JOIN (order_details INNER JOIN products"
                    + " ON products.id_product=order_details.fk_product)"
                    + " ON orders.id_order=order_details.fk_order) "
                    + " ON customers.id_customer=orders.fk_customer "
                    + " GROUP BY customer_name ORDER BY ukupno DESC LIMIT 4;");
            rs = ps.executeQuery();
            while(rs.next()){
                sb.append(rs.getString("customer_name")).append("\t").append(rs.getInt("ukupno")).append("\n");
            }
            System.out.println(sb);
        } catch (SQLException ex) {
             System.out.println(ex);
             throw new WarehouseException("Ne moze se uraditi select za desetu metodu.");
        } finally {
            ResourcesManager.closeResources(rs, ps);
            ResourcesManager.closeConnection(con);
        }
    }
    
    public void jedanaesta() throws WarehouseException, SQLException  {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuilder sb= new StringBuilder();
        try {
            con = ResourcesManager.getConnection();
            ps = con.prepareStatement("SELECT SUM(price_per_unit*quantity) AS max, supplier_name"
                    + " FROM suppliers INNER JOIN (products INNER JOIN order_details"
                    + " ON products.id_product=order_details.fk_product) "
                    + "ON suppliers.id_supplier=products.fk_supplier "
                    + "GROUP BY supplier_name ORDER BY max DESC LIMIT 1");
            rs = ps.executeQuery();
            while(rs.next()){
                sb.append(rs.getString("supplier_name")).append(" je prodao najvise proizvoda sa najvecom vrednoscu: ").append("\t").append(rs.getInt("max")).append("\n");
            }
            System.out.println(sb);
        } catch (SQLException ex) {
             System.out.println(ex);
             throw new WarehouseException("Ne moze se uraditi select za jedanaestu metodu.");
        } finally {
            ResourcesManager.closeResources(rs, ps);
            ResourcesManager.closeConnection(con);
        }
    }
}
