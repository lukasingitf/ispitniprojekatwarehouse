/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.service;

import com.singidunum.projectKDP.dao.OrderDetailsDao;
import com.singidunum.projectKDP.dao.ResourcesManager;
import com.singidunum.projectKDP.data.Customers;
import com.singidunum.projectKDP.data.Employees;
import com.singidunum.projectKDP.data.OrderDetails;
import com.singidunum.projectKDP.data.Orders;
import com.singidunum.projectKDP.data.Products;
import com.singidunum.projectKDP.data.Shippers;
import com.singidunum.projectKDP.exception.WarehouseException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author Uros
 */
public class OrderService {
    
     private static  OrderService instance=null;

    public  OrderService() {
    }
    
    public static  OrderService getInstance(){
        if(instance == null){
            instance= new  OrderService();
        }
        return instance;
    }
    
    public void makeOrder(Customers c, Shippers s, Employees emp, Products p, int quintity, String orderDate) throws  WarehouseException{
         Connection con = null;
        try{
            con = ResourcesManager.getConnection();
            con.setAutoCommit(false);
            
            OrderDetails orderDetails = new OrderDetails(quintity, new Orders(orderDate, c, s, emp), p);           
            OrderDetailsDao.getInstance().insert(orderDetails, con);
            
            con.commit();
            } catch (SQLException ex) {
            ResourcesManager.rollbackTransactions(con);
            System.out.println(ex);
            throw new WarehouseException("Ne moze se napraviti narudzbina.");
        } finally {
            ResourcesManager.closeConnection(con);
        }
    }
    
}
