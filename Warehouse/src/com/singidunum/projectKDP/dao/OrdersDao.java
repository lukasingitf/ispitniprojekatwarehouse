/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.dao;

import com.singidunum.projectKDP.data.Customers;
import com.singidunum.projectKDP.data.Employees;
import com.singidunum.projectKDP.data.Orders;
import com.singidunum.projectKDP.data.Shippers;
import com.singidunum.projectKDP.exception.WarehouseException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Uros
 */
public class OrdersDao { 
    
     private static final OrdersDao instance = new OrdersDao();

    private OrdersDao() {
    }

    public static OrdersDao getInstance() {
        return instance;
    }
    
    public Orders find(int idOrder, Connection con) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        Orders order=null;
        try {
            ps= con.prepareStatement("SELECT * FROM orders WHERE id_order=?");
            ps.setInt(1, idOrder);
            rs=ps.executeQuery();
            if(rs.next()) {
                Shippers shipper = ShippersDao.getInstance().find(rs.getInt("fk_shipper"), con);
                Employees employee = EmployeesDao.getInstance().find(rs.getInt("fk_employee"), con);
                Customers customer = CustomersDao.getInstance().find(rs.getInt("fk_customer"), con);
                
                order= new Orders(idOrder, rs.getString("order_date"), customer, shipper, employee);

            }
        } finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return order; 
    }
    
    public List<Orders> find(Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Orders> ordersList = new ArrayList<>();
        try {
            ps = con.prepareStatement("SELECT * FROM orders");
            
            rs = ps.executeQuery();
            while (rs.next()) {
                Shippers shipper = ShippersDao.getInstance().find(rs.getInt("fk_shipper"), con);
                Employees employee = EmployeesDao.getInstance().find(rs.getInt("fk_employee"), con);
                Customers customer = CustomersDao.getInstance().find(rs.getInt("fk_customer"), con);
                
                Orders order= new Orders(rs.getInt("id_order"), rs.getString("order_date"), customer, shipper, employee);
            }
        } finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return ordersList;
    }
    
    public int insert(Orders order, Connection con) throws SQLException, WarehouseException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        int id = -1;
        try {
            
            ps= con.prepareStatement("INSERT INTO orders(order_date, fk_customer, fk_shipper, fk_employee) VALUES(?,?,?,?);", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, order.getDate());
            
            Customers customer = CustomersDao.getInstance().find(order.getCustomer().getCustomerId(), con);
            if(customer == null){
                throw new WarehouseException("Greska kod insert order. Customer: " + order.getCustomer()+ " ne postoji.");
            }
            
            Shippers shipper = ShippersDao.getInstance().find(order.getShipper().getShipperID(), con);
            if(shipper == null){
                throw new WarehouseException("Greska kod insert order. Shipper: " + order.getShipper()+ " ne postoji.");
            }
            
            Employees employee = EmployeesDao.getInstance().find(order.getEmployee().getEmployeesID(), con);
            if(employee == null){
                throw new WarehouseException("Greska kod insert order. Employee: " + order.getEmployee()+ " ne postoji.");
            }
           
            ps.setInt(2, customer.getCustomerId());
            ps.setInt(3, shipper.getShipperID());
            ps.setInt(4, employee.getEmployeesID());
            ps.executeUpdate();
            rs=ps.getGeneratedKeys();
            rs.next();
            id=rs.getInt(1);
        } finally{
            ResourcesManager.closeResources(null, ps);
        }
        return id;
    }
    
    public void update (Orders order,Connection con) throws SQLException{
        PreparedStatement ps = null;
        try{
            ps = con.prepareStatement("UPDATE orders SET order_date=?, fk_customer=?, fk_shipper=?, fk_employee=? WHERE id_order=?");
            ps.setString(1, order.getDate());
            ps.setInt(2, order.getCustomer().getCustomerId());
            ps.setInt(3, order.getShipper().getShipperID());
            ps.setInt(4, order.getEmployee().getEmployeesID());
            ps.setInt(5, order.getOrderID());
            ps.executeUpdate();
            
            
        } finally{
            ResourcesManager.closeResources(null, ps);
        }
    }
    
    public void delete(Orders order, Connection con) throws SQLException{
        PreparedStatement ps = null;
        try{
            // brisanje ordersDetails
            OrderDetailsDao.getInstance().delete(order, con);
            
         ps = con. prepareStatement("DELETE FROM orders WHERE id_order=?");
            ps.setInt(1, order.getOrderID());
            ps.executeUpdate();
            
        }finally {
            ResourcesManager.closeResources(null, ps);
        }
    }
    
    public void deleteFkProduct(int idOrder, Connection con) throws SQLException{
        PreparedStatement ps = null;
        try{
            OrderDetailsDao.getInstance().deleteFkOrder(idOrder, con);
            
         ps = con. prepareStatement("DELETE FROM orders WHERE id_order=?");
            ps.setInt(1,idOrder);
            ps.executeUpdate();
            
        }finally {
            ResourcesManager.closeResources(null, ps);
        }
    }
    
}

