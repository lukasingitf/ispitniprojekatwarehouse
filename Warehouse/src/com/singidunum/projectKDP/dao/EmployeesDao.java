/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.dao;

import com.singidunum.projectKDP.data.Employees;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Uros
 */
public class EmployeesDao {
    
    private static  EmployeesDao instance=null;

    public  EmployeesDao() {
    }
    
    public static  EmployeesDao getInstance(){
        if(instance == null){
            instance= new  EmployeesDao();
        }
        return instance;
    }
    
    public Employees find(int idEmployee, Connection con) throws SQLException{
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        Employees employee=null;
        
        try {
            ps= con.prepareStatement("SELECT * FROM employees WHERE id_employee=?");
            ps.setInt(1, idEmployee);
            rs=ps.executeQuery();
            if(rs.next()){
                employee = new Employees(idEmployee, rs.getString("last_name"), rs.getString("first_name") , rs.getString("birth_date"));
            }
        }
        finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return employee;
        
    }
    
    public List<Employees> findAll(Connection con) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Employees> employeesList= new ArrayList<>();
        try{
            ps = con.prepareStatement("SELECT * FROM employees");
            rs = ps.executeQuery();
            while(rs.next()){
                Employees employee = new Employees(rs.getInt("id_employee"),rs.getString("last_name"), rs.getString("first_name") , rs.getString("birth_date")); 
                employeesList.add(employee);
            }  
            
        }finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return employeesList;
    }
    
     public int insert( Employees employee, Connection con) throws SQLException {
        PreparedStatement ps= null;
        ResultSet rs= null;
        int id=-1;
        try{
            ps= con.prepareStatement("INSERT INTO employees (last_name, first_name, birth_date) VALUE (?,?,?)", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1,employee.getLastName());
            ps.setString(2,employee.getFirstName());
            ps.setString(3, employee.getBirthDate());
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            rs.next();
            id= rs.getInt(1);
        }finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return id;
    }
     
    public void update(Employees employee, Connection con) throws SQLException{
        PreparedStatement ps = null;
        try{
            ps = con.prepareStatement("UPDATE employees SET last_name=?, first_name=?, birth_name=? WHERE id_employee=?");
            ps.setString(1,employee.getLastName());
            ps.setString(2,employee.getFirstName());
            ps.setString(3, employee.getBirthDate());
            ps.setInt(4, employee.getEmployeesID());
            ps.executeUpdate();
        } finally{
            ResourcesManager.closeResources(null, ps);
        }
    }
    
     public void delete(int idEmployee, Connection con) throws SQLException{
        PreparedStatement ps = null;
        try {
            ps = con.prepareStatement("DELETE FROM employees WHERE id_employee=?");
            ps.setInt(1, idEmployee);
            ps.executeUpdate();
        }finally{
            ResourcesManager.closeResources(null, ps);
        }
    }
    
    
    
}
