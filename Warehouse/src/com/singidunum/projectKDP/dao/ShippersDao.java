/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.dao;

import com.singidunum.projectKDP.data.Shippers;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Uros
 */
public class ShippersDao {
    
    private static  ShippersDao instance=null;

    public  ShippersDao() {
    }
    
    public static  ShippersDao getInstance(){
        if(instance == null){
            instance= new  ShippersDao();
        }
        return instance;
    }
    
    public Shippers find(int idShipper, Connection con) throws SQLException{
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        Shippers shipper=null;
        
        try {
            ps= con.prepareStatement("SELECT * FROM shippers WHERE id_shipper=?");
            ps.setInt(1, idShipper);
            rs=ps.executeQuery();
            if(rs.next()){
                shipper= new Shippers(idShipper, rs.getString("shipper_name"), rs.getString("phone"));
            }
        }
        finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return shipper;
        
    }
    
     public List<Shippers> findAll(Connection con) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Shippers> shippersList= new ArrayList<>();
        try{
            ps = con.prepareStatement("SELECT * FROM shippers");
            rs = ps.executeQuery();
            while(rs.next()){
                Shippers shipper= new Shippers(rs.getInt("id_shipper"), rs.getString("shipper_name"), rs.getString("phone"));
               shippersList.add(shipper);
            }  
            
        }finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return shippersList;
    }
     
    public int insert(Shippers shipper, Connection con) throws SQLException {
        PreparedStatement ps= null;
        ResultSet rs= null;
        int id=-1;
        try{
            ps= con.prepareStatement("INSERT INTO shippers(shipper_name, phone) VALUE (?,?)", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1,shipper.getShipperName());
            ps.setString(2,shipper.getPhone());
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            rs.next();
            id= rs.getInt(1);
        }finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return id;
    }
     
    public void update(Shippers shipper, Connection con) throws SQLException{
        PreparedStatement ps = null;
        try{
            ps = con.prepareStatement("UPDATE shippers SET shipper_name=?, phone=? WHERE id_shipper=?");
            ps.setString(1,shipper.getShipperName());
            ps.setString(2,shipper.getPhone());
            ps.setInt(7, shipper.getShipperID());
            ps.executeUpdate();
        } finally{
            ResourcesManager.closeResources(null, ps);
        }
    }
    
    public void delete(int idShipper, Connection con) throws SQLException{
        PreparedStatement ps = null;
        try {
            ps = con.prepareStatement("DELETE FROM shippers WHERE id_shipper=?");
            ps.setInt(1, idShipper);
            ps.executeUpdate();
        }finally{
            ResourcesManager.closeResources(null, ps);
        }
    }
    
}
