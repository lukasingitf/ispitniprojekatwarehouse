/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.dao;

import com.singidunum.projectKDP.data.Customers;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Uros
 */
public class CustomersDao {
    
    private static  CustomersDao instance=null;

    public  CustomersDao() {
    }
    
    public static  CustomersDao getInstance(){
        if(instance == null){
            instance= new  CustomersDao();
        }
        return instance;
    }
    
    public Customers find(int idCustomer, Connection con) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        Customers customer=null;
        
        try {
            ps= con.prepareStatement("SELECT * FROM customers WHERE id_customer=?");
            ps.setInt(1, idCustomer);
            rs=ps.executeQuery();
            if(rs.next()){
                customer = new Customers(idCustomer, rs.getString("customer_name"), rs.getString("contact_person"), rs.getString("address"), rs.getString("city"), rs.getInt("post_code"), rs.getString("country"));
            }
        }finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return customer;
        
    }
     
    public int insert(Customers customers, Connection con) throws SQLException {
        PreparedStatement ps= null;
        ResultSet rs= null;
        int id=-1;
        try{
            ps= con.prepareStatement("INSERT INTO customers(customer_name, contact_person, address, city, country, post_code) VALUE (?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1,customers.getCustomerName());
            ps.setString(2,customers.getContactPerson());
            ps.setString(3,customers.getAddress());
            ps.setString(4,customers.getCity());
            ps.setString(5,customers.getCountry());
            ps.setInt(6, customers.getPostCode());
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            rs.next();
            id= rs.getInt(1);
        }finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return id;
    }
    
    public void update(Customers customers, Connection con) throws SQLException{
        PreparedStatement ps = null;
        try{
            ps = con.prepareStatement("UPDATE customers SET customer_name=?, contact_person=?, address=?, city=?, country=?, post_code=? WHERE id_custemer=?");
            ps.setString(1,customers.getCustomerName());
            ps.setString(2,customers.getContactPerson());
            ps.setString(3,customers.getAddress());
            ps.setString(4,customers.getCity());
            ps.setString(5,customers.getCountry());
            ps.setInt(6, customers.getPostCode());
            ps.setInt(7, customers.getCustomerId());
            ps.executeUpdate();
        } finally{
            ResourcesManager.closeResources(null, ps);
        }
    }
    
    public void delete(int idCustomer, Connection con) throws SQLException{
        PreparedStatement ps = null;
        try {
            ps = con.prepareStatement("DELETE FROM customers WHERE id_customer=?");
            ps.setInt(1, idCustomer);
            ps.executeUpdate();
        }finally{
            ResourcesManager.closeResources(null, ps);
        }
    }
    
    public void delete(String customerName, Connection con) throws SQLException{
        PreparedStatement ps = null;
        try {
            ps = con.prepareStatement("DELETE FROM customers WHERE id_customer=?");
            ps.setString(1, customerName);
            ps.executeUpdate();
        }finally{
            ResourcesManager.closeResources(null, ps);
        }
    }
    
    public List<Customers> findAll(Connection con) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Customers> customersList= new ArrayList<>();
        try{
            ps = con.prepareStatement("SELECT * FROM customers");
            rs = ps.executeQuery();
            while(rs.next()){
                Customers customer = new Customers(rs.getInt("id_customer"), rs.getString("customer_name"), rs.getString("contact_person"), rs.getString("address"), rs.getString("city"), rs.getInt("post_code"), rs.getString("country"));
                customersList.add(customer);
            }  
            
        }finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return customersList;
    }
      
    
    
}
