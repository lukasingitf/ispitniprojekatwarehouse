/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.dao;

import com.singidunum.projectKDP.data.OrderDetails;
import com.singidunum.projectKDP.data.Orders;
import com.singidunum.projectKDP.data.Products;
import com.singidunum.projectKDP.exception.WarehouseException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Uros
 */
public class OrderDetailsDao {
    
    private static final OrderDetailsDao instance = new OrderDetailsDao();

    private OrderDetailsDao() {
    }

    public static OrderDetailsDao getInstance() {
        return instance;
    }
    
    public OrderDetails find(int idOrderDetails, Connection con) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        OrderDetails orderDetails = null;
        try{
            ps= con.prepareStatement("SELECT * FROM order_details WHERE id_order_details=?");
            ps.setInt(1, idOrderDetails);
            rs= ps.executeQuery();
            if(rs.next()){
                Orders order = OrdersDao.getInstance().find(rs.getInt("fk_order"), con);
                Products product = ProductsDao.getInstance().find(rs.getInt("fk_product"), con);
                orderDetails = new OrderDetails(idOrderDetails, rs.getInt("quantity"), order, product);
            }
        }finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return orderDetails;
    }
    
    public OrderDetails findFkProduct(int idProduct, Connection con) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        OrderDetails orderDetails = null;
        try{
            ps= con.prepareStatement("SELECT * FROM order_details WHERE fk_product=?");
            ps.setInt(1, idProduct);
            rs= ps.executeQuery();
            if(rs.next()){
                Orders order = OrdersDao.getInstance().find(rs.getInt("fk_order"), con);
                Products product = ProductsDao.getInstance().find(rs.getInt("fk_product"), con);
                orderDetails = new OrderDetails(rs.getInt("id_order_details"), rs.getInt("quantity"), order, product);
            }
        }finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return orderDetails;
    }
    
    public List<OrderDetails> find(Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<OrderDetails> orderDetailsList = new ArrayList<>();
        try {
            ps = con.prepareStatement("SELECT * FROM order_details");
            
            rs = ps.executeQuery();
            while (rs.next()) {
                Orders order = OrdersDao.getInstance().find(rs.getInt("fk_order"), con);
                Products product = ProductsDao.getInstance().find(rs.getInt("fk_product"), con);
                OrderDetails orderDetails = new OrderDetails(rs.getInt("id_order_details"), rs.getInt("quantity"), order, product);
            }
        } finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return orderDetailsList;
    }
    
    
    public void insert(OrderDetails orderDetails, Connection con) throws SQLException, WarehouseException{
        PreparedStatement ps = null;
        
        try{
            ps= con.prepareStatement("INSERT INTO order_details (quantity, fk_order, fk_product) VALUES(?,?,?);");
            
            ps.setInt(1, orderDetails.getQuantity());
            Integer fk_order= null;
            if(orderDetails.getOrder()!=null){
                fk_order= OrdersDao.getInstance().insert(orderDetails.getOrder(), con);
            }
            ps.setInt(2, fk_order);
            
            Products product = ProductsDao.getInstance().find(orderDetails.getProduct().getProductId(), con);
            if(product == null) {
                throw  new WarehouseException("Ne postoji product sa id "+orderDetails.getProduct().getProductId());
            }
            ps.setInt(3, product.getProductId());
            ps.executeUpdate();
        }finally {
            ResourcesManager.closeResources(null, ps);
        }
    }
    
    public void update(OrderDetails orderDetails, Connection con) throws SQLException{
        PreparedStatement ps = null;
        try{
            ps= con.prepareStatement("UPDATE order_details SET quantity=?, fk_order=?, fk_product WHERE id_order_details=?");
            ps.setInt(1, orderDetails.getQuantity());
            ps.setInt(2, orderDetails.getOrder().getOrderID());
            ps.setInt(3, orderDetails.getProduct().getProductId());
            ps.executeUpdate();
        }finally {
            ResourcesManager.closeResources(null, ps);
        }
    }
    
     public void delete (Orders order, Connection con) throws SQLException{
         PreparedStatement ps = null;
         try{
             ps= con.prepareStatement("DELETE FROM order_details WHERE fk_order=?");
             ps.setInt(1, order.getOrderID());
             ps.executeUpdate();
         } finally {
             ResourcesManager.closeResources(null, ps);
         }
    }
     
     
     public void delete (Products product, Connection con) throws SQLException{
         PreparedStatement ps = null;
         try{
             ps= con.prepareStatement("DELETE FROM order_details WHERE fk_product=?");
             ps.setInt(1, product.getProductId());
             ps.executeUpdate();
         } finally {
             ResourcesManager.closeResources(null, ps);
         }
    }
     
    
     public void delete (int idOrderDetails, Connection con) throws SQLException{
         PreparedStatement ps = null;
         try{
             ps= con.prepareStatement("DELETE FROM order_details WHERE id_order_details=?");
             ps.setInt(1, idOrderDetails);
             ps.executeUpdate();
         } finally {
             ResourcesManager.closeResources(null, ps);
         }
    }
     
    public void deleteFkOrder (int idOrder, Connection con) throws SQLException{
         PreparedStatement ps = null;
         try{
             ps= con.prepareStatement("DELETE FROM order_details WHERE fk_order=?");
             ps.setInt(1, idOrder);
             ps.executeUpdate();
         } finally {
             ResourcesManager.closeResources(null, ps);
         }
    }
     
}
