/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.dao;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.singidunum.projectKDP.exception.WarehouseException;

/**
 *
 * @author Uros
 */
public class ResourcesManager {
    
    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public static Connection getConnection() throws SQLException{
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost/warehouse?user=root&password=");
        return con;
    }

    public static void closeResources(ResultSet rs, PreparedStatement ps) throws SQLException {
        if(rs!=null){
            rs.close();
        }
        if(ps!=null){
            ps.close();
        }
    }
    
    public static void closeConnection(Connection con) throws WarehouseException{
        if(con!=null){
            try{
                con.close();
            }catch(SQLException e){
                throw new WarehouseException("Neuspesno zatvaranje konekcije sa bazom!", e);
            }
        }
    }
    
    public static void rollbackTransactions(Connection con) throws WarehouseException{
        if (con!=null){
            try {
                con.rollback();
            }catch (SQLException e){
                throw new WarehouseException("Neuspesnan rollback transakcija", e);
            }
        }
    }
     
}
