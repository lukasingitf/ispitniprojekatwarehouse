/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.dao;

import com.singidunum.projectKDP.data.Suppliers;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Uros
 */
public class SuppliersDao {
    
    private static  SuppliersDao instance=null;

    public  SuppliersDao() {
    }
    
    public static  SuppliersDao getInstance(){
        if(instance == null){
            instance= new  SuppliersDao();
        }
        return instance;
    }
    
    public Suppliers find(int idSupplier, Connection con) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        Suppliers supplier=null;
        
        try {
            ps= con.prepareStatement("SELECT * FROM suppliers WHERE id_supplier=?");
            ps.setInt(1, idSupplier);
            rs=ps.executeQuery();
            if(rs.next()){
               supplier = new Suppliers(idSupplier, rs.getString("supplier_name"), rs.getString("contact_person"), rs.getString("address"), rs.getString("city"), rs.getString("country"),rs.getInt("post_code"), rs.getString("phone"));
            }
        }finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return supplier;
        
    }
    
    public List<Suppliers> findAll(Connection con) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Suppliers> suppliersList= new ArrayList<>();
        try{
            ps = con.prepareStatement("SELECT * FROM suppliers");
            rs = ps.executeQuery();
            while(rs.next()){
                Suppliers supplier = new Suppliers(rs.getInt("id_supplier"), rs.getString("supplier_name"), rs.getString("contact_person"), rs.getString("address"), rs.getString("city"), rs.getString("country"),rs.getInt("post_code"), rs.getString("phone"));
                suppliersList.add(supplier);
            }  
            
        }finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return suppliersList;
    }
    
    public int insert(Suppliers supplier, Connection con) throws SQLException {
        PreparedStatement ps= null;
        ResultSet rs= null;
        int id=-1;
        try{
            ps= con.prepareStatement("INSERT INTO suppliers (supplier_name, contact_person, address, city, post_code, country, phone) VALUE (?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1,supplier.getSupplierName());
            ps.setString(2,supplier.getContactPerson());
            ps.setString(3,supplier.getAddress());
            ps.setString(4,supplier.getCity());
            ps.setInt(5, supplier.getPostCode());
            ps.setString(6,supplier.getCountry());
            ps.setString(7, supplier.getPhone());
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            rs.next();
            id= rs.getInt(1);
        }finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return id;
    }
    
    public void update(Suppliers supplier, Connection con) throws SQLException{
        PreparedStatement ps = null;
        try{
            ps = con.prepareStatement("UPDATE suppliers SET supplier_name=?, contact_person=?, address=?, city=?, post_code=?, country=?, phone=? WHERE id_supplier=?");
             ps.setString(1,supplier.getSupplierName());
            ps.setString(2,supplier.getContactPerson());
            ps.setString(3,supplier.getAddress());
            ps.setString(4,supplier.getCity());
            ps.setInt(5, supplier.getPostCode());
            ps.setString(6,supplier.getCountry());
            ps.setString(7, supplier.getPhone());
            ps.setInt(8, supplier.getSupplierID());
            ps.executeUpdate();
        } finally{
            ResourcesManager.closeResources(null, ps);
        }
    }
    
    public void delete(int idSupplier, Connection con) throws SQLException{
        PreparedStatement ps = null;
        try {
            ps = con.prepareStatement("DELETE FROM suppliers WHERE id_supplier=?");
            ps.setInt(1, idSupplier);
            ps.executeUpdate();
        }finally{
            ResourcesManager.closeResources(null, ps);
        }
    }
}
