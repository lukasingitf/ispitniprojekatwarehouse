/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.dao;

import com.singidunum.projectKDP.data.Products;
import com.singidunum.projectKDP.data.Suppliers;
import com.singidunum.projectKDP.exception.WarehouseException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Uros
 */
public class ProductsDao {
    
    private static  ProductsDao instance=null;

    public  ProductsDao() {
    }
    
    public static  ProductsDao getInstance(){
        if(instance == null){
            instance= new  ProductsDao();
        }
        return instance;
    }
    
    public Products find(int idProduct, Connection con) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        Products product= null;
        try {
            ps= con.prepareStatement("SELECT * FROM products WHERE id_product=?");
            ps.setInt(1, idProduct);
            rs = ps.executeQuery();
            if(rs.next()){
                Suppliers supplier = SuppliersDao.getInstance().find(rs.getInt("fk_supplier"), con);
                product = new Products(idProduct, rs.getString("product_name"), rs.getString("product_category"), rs.getInt("price_per_unit"), supplier);
            }
        } finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return product;
    }
    
    public List<Products> findAll(Connection con) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Products> productsList= new ArrayList<>();
        try{
            ps = con.prepareStatement("SELECT * FROM products");
            rs = ps.executeQuery();
            while(rs.next()){
                Suppliers supplier = SuppliersDao.getInstance().find(rs.getInt("fk_supplier"), con);
                Products product = new Products(rs.getInt("id_product"), rs.getString("product_name"), rs.getString("product_category"), rs.getInt("price_per_unit"), supplier);
                productsList.add(product);
            }  
            
        }finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return productsList;
    }
    
     public List<Products> findAllProductSupplier(int idSupplier, Connection con) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Products> productsList= new ArrayList<>();
        try{
            ps = con.prepareStatement("SELECT * FROM products WHERE fk_supplier=?");
            ps.setInt(1, idSupplier);
            rs = ps.executeQuery();
            while(rs.next()){
                Suppliers supplier = SuppliersDao.getInstance().find(rs.getInt("fk_supplier"), con);
                Products product = new Products(rs.getInt("id_product"), rs.getString("product_name"), rs.getString("product_category"), rs.getInt("price_per_unit"), supplier);
                productsList.add(product);
            }  
            
        }finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return productsList;
    }
    
    public int insert(Products product, Connection con) throws SQLException, WarehouseException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        int id=-1;
        try {
            ps = con.prepareStatement("INSERT INTO products(product_name, product_category, price_per_unit, fk_supplier) VALUES(?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, product.getProductName());
            ps.setString(2, product.getProductCategory());
            ps.setInt(3, product.getPricePerUnit());
            
            Suppliers supplier = SuppliersDao.getInstance().find(product.getSupplier().getSupplierID(), con);
            if(supplier == null) {
                throw new WarehouseException("Greska kod insert Product. Supplier "+ product.getSupplier()+" ne postoji.");
                
            }
            ps.setInt(4,supplier.getSupplierID());
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            rs.next();
            id=rs.getInt(1);
            
        }finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return id;
    }
    
    public void update(Products product, Connection con) throws SQLException{
        PreparedStatement ps = null;
        try{
            ps= con.prepareStatement("UPDATE products SET product_name=?, product_category=?, price_per_unit=?, fk_supplier=? WHERE id_product=?");
            ps.setString(1, product.getProductName());
            ps.setString(2, product.getProductCategory());
            ps.setInt(3, product.getPricePerUnit());
            ps.setInt(4, product.getSupplier().getSupplierID());
            ps.setInt(5, product.getProductId());
            ps.executeUpdate();
        }finally {
            ResourcesManager.closeResources(null, ps);
        }
    }
    
     public void delete (Products product, Connection con) throws SQLException{
        PreparedStatement ps = null;
        try{
            
            //delete orders of the product
//            OrderDetailsDao.getInstance().delete(product, con);

            //delete product
            
            ps = con. prepareStatement("DELETE FROM products WHERE id_product=?");
            ps.setInt(1, product.getProductId());
            ps.executeUpdate();
            
        }finally {
            ResourcesManager.closeResources(null, ps);
        }
    }
}
