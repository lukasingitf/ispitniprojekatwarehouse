/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.data;

/**
 *
 * @author Uros
 */
public class Orders {
    private int orderID;
    private String date;
    private Customers customer;
    private Shippers shipper;
    private Employees employee;

    public Orders(int orderID, String date, Customers customer, Shippers shipper, Employees employee) {
        this.orderID = orderID;
        this.date = date;
        this.customer = customer;
        this.shipper = shipper;
        this.employee = employee;
    }

    public Orders(String date, Customers customer, Shippers shipper, Employees employee) {
        this.date = date;
        this.customer = customer;
        this.shipper = shipper;
        this.employee = employee;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Customers getCustomer() {
        return customer;
    }

    public void setCustomer(Customers customer) {
        this.customer = customer;
    }

    public Shippers getShipper() {
        return shipper;
    }

    public void setShipper(Shippers shipper) {
        this.shipper = shipper;
    }

    public Employees getEmployee() {
        return employee;
    }

    public void setEmployee(Employees employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "Orders{" + "orderID=" + orderID + ", date=" + date + ", customer=" + customer + ", shipper=" + shipper + ", employee=" + employee + '}';
    }
    
    
}
