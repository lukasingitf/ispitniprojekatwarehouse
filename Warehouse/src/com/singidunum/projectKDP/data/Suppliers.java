/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.data;

/**
 *
 * @author Uros
 */
public class Suppliers {
    
    private int supplierID;
    private String supplierName;
    private String contactPerson;
    private String address;
    private String city;
    private String country;
    private int postCode;
    private String phone;

    public Suppliers(int supplierID, String supplierName, String contactPerson, String address, String city, String country, int postCode, String phone) {
        this.supplierID = supplierID;
        this.supplierName = supplierName;
        this.contactPerson = contactPerson;
        this.address = address;
        this.city = city;
        this.country = country;
        this.postCode = postCode;
        this.phone = phone;
    }

    public Suppliers(String supplierName, String contactPerson, String address, String city, String country, int postCode, String phone) {
        this.supplierName = supplierName;
        this.contactPerson = contactPerson;
        this.address = address;
        this.city = city;
        this.country = country;
        this.postCode = postCode;
        this.phone = phone;
    }

    public int getSupplierID() {
        return supplierID;
    }

    public void setSupplierID(int supplierID) {
        this.supplierID = supplierID;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getPostCode() {
        return postCode;
    }

    public void setPostCode(int postCode) {
        this.postCode = postCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Suppliers{" + "supplierID=" + supplierID + ", supplierName=" + supplierName + ", contactPerson=" + contactPerson + ", address=" + address + ", city=" + city + ", country=" + country + ", postCode=" + postCode + ", phone=" + phone + '}';
    }
    
    
    
    
}
