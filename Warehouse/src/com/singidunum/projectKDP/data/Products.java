/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.data;

/**
 *
 * @author Uros
 */
public class Products {
    
    private int productId;
    private String productName;
    private String productCategory;
    private int pricePerUnit;
    private Suppliers supplier;

    public Products(int productId, String productName, String productCategory, int pricePerUnit, Suppliers suplier) {
        this.productId = productId;
        this.productName = productName;
        this.productCategory = productCategory;
        this.pricePerUnit = pricePerUnit;
        this.supplier = suplier;
    }

    public Products(String productName, String productCategory, int pricePerUnit, Suppliers suplier) {
        this.productName = productName;
        this.productCategory = productCategory;
        this.pricePerUnit = pricePerUnit;
        this.supplier = suplier;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public int getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(int pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    public Suppliers getSupplier() {
        return supplier;
    }

    public void setSupplier(Suppliers supplier) {
        this.supplier = supplier;
    }

    @Override
    public String toString() {
        return "Products{" + "productId=" + productId + ", productName=" + productName + ", productCategory=" + productCategory + ", pricePerUnit=" + pricePerUnit + ", supplier=" + supplier + '}';
    }
    
    
    
}
