/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.data;

/**
 *
 * @author Uros
 */
public class OrderDetails {
    
    private int orderDetailsId;
    private int quantity;
    private Orders order;
    private Products product;

    public OrderDetails(int orderDetailsId, int quantity, Orders order, Products product) {
        this.orderDetailsId = orderDetailsId;
        this.quantity = quantity;
        this.order = order;
        this.product = product;
    }

    public OrderDetails(int quantity, Orders order, Products product) {
        this.quantity = quantity;
        this.order = order;
        this.product = product;
    }

    public int getOrderDetailsId() {
        return orderDetailsId;
    }

    public void setOrderDetailsId(int orderDetailsId) {
        this.orderDetailsId = orderDetailsId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Orders getOrder() {
        return order;
    }

    public void setOrder(Orders order) {
        this.order = order;
    }

    public Products getProduct() {
        return product;
    }

    public void setProduct(Products product) {
        this.product = product;
    }

    @Override
    public String toString() {
        return "OrderDetails{" + "orderDetailsId=" + orderDetailsId + ", quantity=" + quantity + ", order=" + order + ", product=" + product + '}';
    }
    
    
}
