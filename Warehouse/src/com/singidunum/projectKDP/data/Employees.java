/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.singidunum.projectKDP.data;

/**
 *
 * @author Uros
 */
public class Employees {
    
    private int employeesID;
    private String lastName;
    private String firstName;
    private String birthDate;

    public Employees(int employeesID, String lastName, String firstName, String birthDate) {
        this.employeesID = employeesID;
        this.lastName = lastName;
        this.firstName = firstName;
        this.birthDate = birthDate;
    }

    public Employees(String lastName, String firstName, String birthDate) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.birthDate = birthDate;
    }

    public int getEmployeesID() {
        return employeesID;
    }

    public void setEmployeesID(int employeesID) {
        this.employeesID = employeesID;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public String toString() {
        return "Employees{" + "employeesID=" + employeesID + ", lastName=" + lastName + ", firstName=" + firstName + ", birthDate=" + birthDate + '}';
    }
    
    
}
