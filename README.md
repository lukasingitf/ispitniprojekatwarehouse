# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

 Ispitni zadatak, junski ispitni rok 2017. Predmet KDP.
 1.0.1.





### Who do I talk to? ###

 Luka Kovacevic br.indeksa 2015203008 @ Singidunum Universety
 Mentor dr. Milan Cabarkapa

### Summary ###

Projektni zadatak za zavšni ispit 2016/17.
Potrebno je kreirati bazu podataka Warehouse koja ima sledeće tabele i veze (strani ključevi u
tabelama su italic):
Tabela Customers (polja: CustomerId, CustomerName, ContactPerson, Address, City,
PostCode, Country)
Tabela Employees(polja: EmployeeId, LastName, FirstName, BirthDate)
Tabela Suppliers(polja: SupplierId, SupplierName, ContactPerson, Address, City, PostCode,
Country, Phone)
Tabela Shippers(polja: ShipperId, ShipperName, Phone)
Tabela Products(polja: ProductId, ProductName, SuplierID, ProductCategory, PricePerUnit)
Tabela Orders(polja: OrderId, OrderDate, CustomerId, EmployeeId, ShipperId)
Tabela OrderDetails(polja: OrderDetailsId, OrderId, ProductId, Quantity)
Potrebno je napraviti Java program za rad sa bazom podataka Warehouse.
Prvo treba kreirati Data sloj u paketu com.singidunum.projectKDP.data u kome se nalaze klase
od kojih će biti instancirani takozvani Data Entity Objects u koje se mapira svaka tabela u bazi.
To su obične Java klase koje imaju privatne atribute iste kao i polja u tabelama, konstruktor, getere
i set-ere. Treba implementirati i metodu toString() radi lakšeg dohvatanja tekstualnog opisa
svake instance klasa iz Data sloja. Radi opštosti u daljem tekstu sve data klase ćemo zvati
DataClass, a njene instance dataObject.
Nakon ovoga potrebno je kreirati Dao sloj u paketu com.singidunum.projectKDP.dao u kome se
nalaze Dao klase za rad sa bazom podataka.
Svaka od Dao klasa treba da implementira svoj interfejs sa sledećim metodama:
1* Metoda `public void create(Connection con, DataClass dataObject) throws SQLException`
U bazu podataka dodaje novi dataObject čija je klasa implementirana u Data sloju.
Kao parametre prima konekciju na bazu podataka i sebi svojstven objekat iz Data sloja u koji su
enkapsulirani podaci koje treba upisati u odgovarajuću tabelu.
2* Metoda `public void update(Connection con, DataClass dataObject) throws SQLException`
Ažurira podatke o nekom zapisu u bazi podataka.
Kao parametre prima konekciju na bazu podataka i objekat tipa dataObject sa ažuriranim
podacima. Metoda treba da ažurira dataObject čiji je Id jednak dataObject.getDataObjectId()
3* Metoda `public void delete(Connection con, int dataObjectId) throws SQLException`
Briše zapis iz baze sa zadatim dataObjectId.
4* Metoda `public dataObject find(Connection con, int dataObjectId) throws SQLException`
Nalazi dataObject sa zadatim dataObjectId u bazi podataka, rezultat enkapsulira u objekat tipa
dataObject.
Ako ne postoji zapis sa zadatim dataObjectId metoda treba da vrati null.
5* Metoda `public List<dataObject> findAll(Connection con) throws SQLException`
Nalazi sve dataObject koji se nalaze u bazi podataka, kao rezultat vraća listu objekata tipa
dataObject.
Ako je baza prazna i nema ni jedan dataObject treba vratiti praznu listu (ne null).
Potrebno je implementirati servisne klase u Service sloju u paketu
com.singidunum.projectKDP.service koje za svaki od entiteta u bazi i data/dao sloju
implementiraju transaction management, kao i aplikativnu logiku vezanu za Warehouse bazu.
Videti primer projekta OnlineShop rađenog na vežbama. Sve servisne klase mogu koristiti
Dao i Data klase.
Potrebno je implementirati i servisnu klasu AdvancedService u Service sloju koja treba da se
nalazi u paketu com.singidunum.projectKDP.service. Ova servisna klasa implementira složenije
operacije nad bazom podataka i može koristiti Data, Dao i Service klase. Potrebno je omogućiti
sledeće operacije (metode):
1. Izlistati svakog Customer-a i sve njihove Order-e jednog ispod drugog u formatu
CustomerName OrderId
Customer-e sortirati po abecednom redu.
2. Izlistati sve Product-e za čije dobavljanje je nadležan Supplier sa datim SupplierId
3. Izlistati sve Product-e koje je dostavio Shipper sa datim ShipperId
4. Izračunati ukupnu cenu svih narudžbina
5. Izračunati cenu narudžbina (Order-a) koje je naručio Customer sa datim CustomerId
6. Izračunati cenu narudžbina (Order-a) koje je dostavio Shipper sa datim ShipperId
7. Izračunati cenu narudžbina koje je dobavio Supplier sa datim SupplierId
8. Pronaći onog zaposlenog kod koga je naručena najveća vrednost robe
9. Pronaći 2 Product-a koji su najviše puta naručivani
10. Pronaći 4 najbolja Customer-a sa stanovišta cene naručene robe
11. Pronaći Supplier-a koji je dostavio najviše robe koja je prodata (kao kriterijum koristiti
ukupnu cenu svih prodatih proizvoda)
Potrebno je napuniti bazu svim potrebnim podacima da bi se mogle testirati sve tražene
funkcionalnosti. Ispisivati sve rezultate na standardnom izlazu u konzoli. Drugim rečima, potrebno 
je iz glavnog Java programa demostrirati rad sa bazom podataka i probati sve operacije koje su
implementirane u Dao i Service sloju.
Takođe, potrebno je upload-ovati sve kodove na BitBucket source control i instalirati Git na
računarima. Potrebno je otvoriti bitbucket account, kreirati repozitorijum i dodati profesora na
projekat. BitBucket ID profesora je mcabarkapa.
Bitbucket username je dovoljan da admin pridruži timu druge članove (u ovom slučaju bitbucket
id profesora). Ovaj repozitorijum treba shvatiti kao testni, tako da se na njemu može vežbati
kloniranje (povezivanje sa remote repozitorijumom i svlačenje koda kod sebe lokalno), čitanje
(učitavanje projekta u svoj IDE), menjanje koda, komitovanje, push-ovanje, pull-ovanje. Studenti
moraju imati instaliran Git na svojoj mašini. Najbolje je skinuti Git Bash alat sa Bitbucket sajta
(možete instalaciju preuzeti i od profesora ili skinuti), to je alat sa komandama iz konzolne linije i
sasvim je dovoljan. Potrebno je imati samo master granu u okviru projekata. Preko web browsera
možete da se ulogujete na Bitbucket (prvo kreirati nalog) i u svakom trenutku preko web
aplikacije možete da pratite kakav je Source na Remote repozitorujumu na Bitbucket-u (commit
samo vrši lokalne promene na vašem lokalnom repozitorijumu, commit stavlja fajlove u niz
promena koje onda sa push guramo na remote repozitorum, Tek kad je kod push-ovan na remote
repozitorijum, onda svi kolaboranti na projektu mogu da vide unete promene tako što će uraditi
pull). Kada svoje promene hoćete da stavite na remote repozitorijum, neki pravilan redosled
koraka je sledeći:
git status
git add .
git commit -m "some comment"
git pull
git push
git status vam pokazuje šta je lokalno promenjeno, git add . stavlja sve promene u red za commit,
git commit vrši lokalan komit promena, git pull dovlači promene sa remote master grane i mergeuje
promene sa onim što je prethodno lokalno komitovano sa git commit. git push gura lokalne
promene na remote master granu. Ovaj redosled koraka je bitan jer sprečava konflikte prilikom
push-ovanja! Znači, prvo add, pa commit, pa pull, pa push.
Dokumentacija:
https://confluence.atlassian.com/bitbucket/bitbucket-cloud-documentation-home-
221448814.html
Napomena:
Ukoliko u zadacima nešto nije jasno definisano, usvojiti razumnu pretpostavku i na temeljima te
pretpostavke nastaviti rešavanje zadataka.